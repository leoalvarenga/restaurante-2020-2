package br.ucsal.bes20202.testequalidade.restaurante.business;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;

import br.ucsal.bes20202.testequalidade.restaurante.builder.MesaBuilder;
import br.ucsal.bes20202.testequalidade.restaurante.domain.Comanda;
import br.ucsal.bes20202.testequalidade.restaurante.domain.Mesa;
import br.ucsal.bes20202.testequalidade.restaurante.enums.SituacaoMesaEnum;
import br.ucsal.bes20202.testequalidade.restaurante.exception.MesaOcupadaException;
import br.ucsal.bes20202.testequalidade.restaurante.exception.RegistroNaoEncontrado;
import br.ucsal.bes20202.testequalidade.restaurante.persistence.ComandaDao;
import br.ucsal.bes20202.testequalidade.restaurante.persistence.ItemDao;
import br.ucsal.bes20202.testequalidade.restaurante.persistence.MesaDao;

public class RestauranteBOIntegradoTest {

	/**
	 * Método a ser testado:
	 * 
	 * public void abrirComanda(Integer numeroMesa) throws RegistroNaoEncontrado,
	 * MesaOcupadaException.
	 * 
	 * Verificar se a abertura de uma comanda para uma mesa livre apresenta sucesso.
	 * 
	 * Atenção:
	 * 
	 * 1. Crie um builder para instanciar a classe Mesa;
	 * 
	 * 2. Como o método abrirComanda é void, você deverá utilizar o ComandaDAO para
	 * verificar se o método teve sucesso. É necessário verificar se a comanda foi
	 * incluída para a mesa informada.
	 * @throws MesaOcupadaException 
	 * @throws RegistroNaoEncontrado 
	 * 
	 */
	
	
	
	@Test
	public void abrirComandaMesaLivre() throws MesaOcupadaException, RegistroNaoEncontrado {
		
		MesaDao mesaDao = new MesaDao();
		
		Mesa mesa = MesaBuilder.aMesa().withNumero(2).withCapacidade(8).withSituacao(SituacaoMesaEnum.LIVRE).build();
		
		mesaDao.incluir(mesa);
		
		ComandaDao comandaDao = new ComandaDao();
		
		ItemDao itemDao = new ItemDao();
		
		RestauranteBO restauranteBO = new RestauranteBO(mesaDao, comandaDao, itemDao);
		
		restauranteBO.abrirComanda(2);
		
		List<Comanda> listaDeComandas = comandaDao.obterComandasPorNumeroMesa(2);
		
		assertTrue(listaDeComandas.size() > 0);
		
	}
}
