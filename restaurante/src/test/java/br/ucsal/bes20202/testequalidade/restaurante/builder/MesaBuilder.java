package br.ucsal.bes20202.testequalidade.restaurante.builder;

import br.ucsal.bes20202.testequalidade.restaurante.domain.Mesa;
import br.ucsal.bes20202.testequalidade.restaurante.enums.SituacaoMesaEnum;

public class MesaBuilder {
	
	public static final Integer DEFAULT_NUMERO = 1;
	public static final Integer DEFAULT_CAPACIDADE = 4;
	public static final SituacaoMesaEnum DEFAULT_SITUACAO = SituacaoMesaEnum.LIVRE;
	
	private Integer numero = DEFAULT_NUMERO;
	private Integer capacidade = DEFAULT_CAPACIDADE;
	private SituacaoMesaEnum situacao = DEFAULT_SITUACAO;
	
	public MesaBuilder() {}
	
	public static MesaBuilder aMesa() {
		return new MesaBuilder();
	}
	
	public MesaBuilder withNumero(Integer numero) {
		this.numero = numero;
		return this;
	}
	
	public MesaBuilder withCapacidade(Integer capacidade) {
		this.capacidade = capacidade;
		return this;
	}
	
	public MesaBuilder withSituacao(SituacaoMesaEnum situacao) {
		this.situacao = situacao;
		return this;
	}
	
	public MesaBuilder but() {
		return MesaBuilder.aMesa().withNumero(numero).withCapacidade(capacidade).withSituacao(situacao);
	}
	
	public Mesa build() {
		Mesa mesa = new Mesa();
		mesa.setCapacidade(this.capacidade);
		mesa.setNumero(this.numero);
		mesa.setSituacao(this.situacao);
		return mesa;
	}

}
