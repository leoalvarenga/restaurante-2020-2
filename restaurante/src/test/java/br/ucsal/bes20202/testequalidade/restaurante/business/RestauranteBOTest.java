package br.ucsal.bes20202.testequalidade.restaurante.business;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.internal.matchers.*;

import br.ucsal.bes20202.testequalidade.restaurante.builder.MesaBuilder;
import br.ucsal.bes20202.testequalidade.restaurante.domain.Comanda;
import br.ucsal.bes20202.testequalidade.restaurante.domain.Mesa;
import br.ucsal.bes20202.testequalidade.restaurante.exception.MesaOcupadaException;
import br.ucsal.bes20202.testequalidade.restaurante.exception.RegistroNaoEncontrado;
import br.ucsal.bes20202.testequalidade.restaurante.persistence.ComandaDao;
import br.ucsal.bes20202.testequalidade.restaurante.persistence.ItemDao;
import br.ucsal.bes20202.testequalidade.restaurante.persistence.MesaDao;

public class RestauranteBOTest {

	/**
	 * Método a ser testado:
	 * 
	 * public void abrirComanda(Integer numeroMesa) throws RegistroNaoEncontrado,
	 * MesaOcupadaException.
	 * 
	 * Verificar se a abertura de uma comanda para uma mesa livre apresenta sucesso.
	 * 
	 * Atenção:
	 * 
	 * 1. Crie um builder para instanciar a classe Mesa;
	 * 
	 * 2. NÃO é necesário fazer o mock para o getSituacao;
	 * 
	 * 3. NÃO é necesário fazer o mock para o construtor de Comanda;
	 * 
	 * 4. Como o método abrirComanda é void, você deverá usar o verify para se
	 * certificar do sucesso o não da execução do teste. Não é necessário garantir
	 * que a comanda incluída é para a mesa informada, basta verificar se uma
	 * comanda foi incluída.
	 * @throws MesaOcupadaException 
	 * @throws RegistroNaoEncontrado 
	 * 
	 */

	@Test
	public void abrirComandaMesaLivre() throws RegistroNaoEncontrado, MesaOcupadaException {

		MesaDao mesaDaoFake = Mockito.mock(MesaDao.class);

		Mesa mesa = MesaBuilder.aMesa().build();
		
		Mockito.when(mesaDaoFake.obterPorNumero(Mockito.anyInt())).thenReturn(mesa);

		ComandaDao comandaDaoFake = Mockito.mock(ComandaDao.class);

		ItemDao itemDaoFake = Mockito.mock(ItemDao.class);

		RestauranteBO restauranteBO = new RestauranteBO(mesaDaoFake, comandaDaoFake, itemDaoFake);

		restauranteBO.abrirComanda(3);

		Mockito.verify(comandaDaoFake).incluir(Mockito.any(Comanda.class));
	}
}
